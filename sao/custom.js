(function() {
    'use strict';
}());

Sao.config.limit = 1000;
Sao.config.display_size = 50;
Sao.config.roundup = {};
Sao.config.roundup.url = 'https://support.m9s.biz/';
Sao.config.title = 'MBSolutions ERP';
Sao.config.icon_colors = '#43a299,#555753,#cc0000'.split(',');
Sao.config.bus_timeout = 10 * 60 * 1000;
