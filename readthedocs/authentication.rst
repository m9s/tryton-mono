:orphan:

.. _index-authentication:

Authentication
==============

`SMS </projects/modules-authentication-sms/en/6.0>`_
    Authentication per SMS.

`LDAP </projects/modules-ldap-authentication/en/6.0>`_
    Authentication per LDAP.
