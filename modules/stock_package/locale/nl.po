#
msgid ""
msgstr "Content-Type: text/plain; charset=utf-8\n"

msgctxt "field:stock.configuration,package_sequence:"
msgid "Package Sequence"
msgstr "Pakketvolgorde"

msgctxt "field:stock.configuration.sequence,package_sequence:"
msgid "Package Sequence"
msgstr "Pakketvolgorde"

msgctxt "field:stock.move,package:"
msgid "Package"
msgstr "Pakket"

msgctxt "field:stock.package,children:"
msgid "Children"
msgstr "Onderliggende niveaus"

msgctxt "field:stock.package,code:"
msgid "Code"
msgstr "Code"

msgctxt "field:stock.package,company:"
msgid "Company"
msgstr ""

msgctxt "field:stock.package,moves:"
msgid "Moves"
msgstr "Boekingen/bewegingen"

#, fuzzy
msgctxt "field:stock.package,packaging_weight:"
msgid "Packaging Weight"
msgstr "Pakket"

msgctxt "field:stock.package,packaging_weight_digits:"
msgid "Packaging Weight Digits"
msgstr ""

msgctxt "field:stock.package,packaging_weight_uom:"
msgid "Packaging Weight Uom"
msgstr ""

msgctxt "field:stock.package,parent:"
msgid "Parent"
msgstr "Bovenliggend niveau"

msgctxt "field:stock.package,shipment:"
msgid "Shipment"
msgstr "levering"

msgctxt "field:stock.package,state:"
msgid "State"
msgstr "Status"

msgctxt "field:stock.package,type:"
msgid "Type"
msgstr "Type"

msgctxt "field:stock.package.type,name:"
msgid "Name"
msgstr "Naam"

#, fuzzy
msgctxt "field:stock.package.type,packaging_weight:"
msgid "Packaging Weight"
msgstr "Pakket"

msgctxt "field:stock.package.type,packaging_weight_digits:"
msgid "Packaging Weight Digits"
msgstr ""

msgctxt "field:stock.package.type,packaging_weight_uom:"
msgid "Packaging Weight Uom"
msgstr ""

msgctxt "field:stock.shipment.in.return,packages:"
msgid "Packages"
msgstr "pakketten"

msgctxt "field:stock.shipment.in.return,root_packages:"
msgid "Packages"
msgstr "pakketten"

msgctxt "field:stock.shipment.out,packages:"
msgid "Packages"
msgstr "pakketten"

msgctxt "field:stock.shipment.out,root_packages:"
msgid "Packages"
msgstr "pakketten"

msgctxt "help:stock.package,packaging_weight:"
msgid "The weight of the package when empty."
msgstr ""

msgctxt "help:stock.package.type,packaging_weight:"
msgid "The weight of the package when empty."
msgstr ""

msgctxt "model:ir.action,name:act_package_type_form"
msgid "Package Types"
msgstr "Pakkettypen"

msgctxt "model:ir.action,name:report_package_label"
msgid "Package Labels"
msgstr "Pakketetiketten"

msgctxt "model:ir.message,text:msg_package_mismatch"
msgid "To process shipment \"%(shipment)s\", you must pack all its moves."
msgstr ""
"Om verzending \"%(shipment)s\" te verwerken, moet u alle bewegingen "
"inpakken."

msgctxt "model:ir.rule.group,name:rule_group_package_companies"
msgid "User in companies"
msgstr ""

msgctxt "model:ir.sequence,name:sequence_package"
msgid "Stock Package"
msgstr "verpakking voorraad"

msgctxt "model:ir.sequence.type,name:sequence_type_package"
msgid "Stock Package"
msgstr "verpakking voorraad"

msgctxt "model:ir.ui.menu,name:menu_package_form"
msgid "Package Types"
msgstr "Pakkettypen"

msgctxt "model:stock.package,name:"
msgid "Stock Package"
msgstr "verpakking voorraad"

msgctxt "model:stock.package.type,name:"
msgid "Stock Package Type"
msgstr "Type voorraadpakket"

msgctxt "report:stock.package.label:"
msgid "Package:"
msgstr "Pakket:"

msgctxt "report:stock.package.label:"
msgid "Shipment:"
msgstr "Verzending:"

msgctxt "selection:stock.package,state:"
msgid "Closed"
msgstr "Afgesloten"

msgctxt "selection:stock.package,state:"
msgid "Open"
msgstr "Open"

msgctxt "view:stock.package.type:"
msgid "Measurements"
msgstr ""

msgctxt "view:stock.package:"
msgid "Measurements"
msgstr ""
